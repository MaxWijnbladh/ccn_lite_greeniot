#!/bin/sh
set -e
cd paho.mqtt.c
make -f Makefile
cd ../src
make clean all -f Makefile
:
