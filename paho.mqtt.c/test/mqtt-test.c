#include "stdio.h"
#include <stdarg.h>
#include "stdlib.h"
#include "string.h"
#include "MQTTClient.h"
#include <mongoc.h>
#include <bson.h>
#include <bcon.h>
#include <json/json.h>

#define ADDRESS     "mqtt.greeniot.it.uu.se"
#define CLIENTID    "SensorSub"
#define TOPIC       "iot-2/type/greenviot/id/d08c5ee90009/evt/status/fmt/json"
#define QOS         1
#define TIMEOUT     10000L

volatile MQTTClient_deliveryToken deliveredtoken;


void delivered(void *context, MQTTClient_deliveryToken dt)
{
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

void saveToFile(const char *filepath, const char *data)
{
    FILE *fp = fopen(filepath, "ab");
    if (fp != NULL)
    {
        fputs(data, fp);
        fclose(fp);
    }
}

char* concat(int count, ...)
{
    va_list ap;
    int i;

    // Find required length to store merged string
    int len = 1; // room for NULL
    va_start(ap, count);
    for(i=0 ; i<count ; i++)
        len += strlen(va_arg(ap, char*));
    va_end(ap);

    // Allocate memory to concat strings
    char *merged = calloc(sizeof(char),len);
    int null_pos = 0;

    // Actually concatenate strings
    va_start(ap, count);
    for(i=0 ; i<count ; i++)
    {
        char *s = va_arg(ap, char*);
        strcpy(merged+null_pos, s);
        null_pos += strlen(s);
    }
    va_end(ap);

    return merged;
}

char * json_parse(json_object * jobj) {
   enum json_type type;
   char * seqno = "";
   char * sensorid = "";
   json_object_object_foreach(jobj, key, val) {

       json_object * inner = json_tokener_parse(json_object_get_string(val));
       json_object_object_foreach(inner, keyinner, valinner) {
        //printf("type: %u\n", json_object_get_type(valinner));
        printf("key inner: %s\n", keyinner);

        if(strcmp(keyinner, "seq_no") == 0) {
            seqno = concat(2, json_object_get_string(valinner), "/");
        } else if(strcmp(keyinner, "Def Route") == 0) {
            sensorid = concat(2, json_object_get_string(valinner), "/");
        }
    }
}

return concat(2, sensorid, seqno);
}


int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    int i;
    char* payloadptr;
    mongoc_client_t      *mongoClient;
    mongoc_database_t    *database;
    mongoc_collection_t  *collection;
    bson_t               *command, *doc, reply, *insert;
    bson_error_t          error;
    char                 *str;
    bool                  retval;
    char *json;
    printf("Before\n");
    mongoc_init ();
    printf("INIT\n");
    mongoClient = mongoc_client_new ("mongodb://projectcs:projectcspw@130.238.15.228:27017/test");

    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("   message: ");

    payloadptr = message->payload;
    json_object * jobj = json_tokener_parse((char *) payloadptr);
    char* name = json_parse(jobj);
    char* prefix = concat(3, "'greenIoT/", name, "'");
    printf("prefix: %s\n", prefix);
    //char* fileName = concat(3, "greenIoT", name ,".txt");
    char* fileName = "temp.txt";
    saveToFile(fileName, (char *) payloadptr);

    database = mongoc_client_get_database (mongoClient, "test");
    collection = mongoc_client_get_collection (mongoClient, "test", "test");

    doc = bson_new_from_json ((char *) message->payload, -1, &error);

    printf ("JSON string: %s\n", (char *) message->payload);


    if (!mongoc_collection_insert (collection, MONGOC_INSERT_NONE, doc, NULL, &error)) {
        fprintf (stderr, "%s\n", error.message);
    }

    char *cmd = concat(3, "$CCNL_HOME/src/util/ccn-lite-mkC -i temp.txt -s ccnx2015 ", prefix, " -o $CCNL_HOME/test/ccntlv/gringo.ccntlv");
    system(cmd);
    cmd = "$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-max.sock addContentToCache $CCNL_HOME/test/ccntlv/gringo.ccntlv";
    system(cmd);

    remove(fileName);


    printf("inserted\n");

    bson_destroy (doc);
    mongoc_client_destroy (mongoClient);
    mongoc_cleanup ();

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}


void connlost(void *context, char *cause)
{
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);
}

int main(int argc, char* argv[])
{
    MQTTClient client;
    MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    int rc;
    int ch;


    MQTTClient_create(&client, ADDRESS, CLIENTID,
        MQTTCLIENT_PERSISTENCE_NONE, NULL);
    conn_opts.keepAliveInterval = 20;
    conn_opts.cleansession = 1;

    MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered);

    if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
    {
        printf("Failed to connect, return code %d\n", rc);
        exit(-1);       
    }
    printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
       "Press Q<Enter> to quit\n\n", TOPIC, CLIENTID, QOS);
    MQTTClient_subscribe(client, TOPIC, QOS);

    do 
    {
        ch = getchar();
    } while(ch!='Q' && ch != 'q');


    MQTTClient_disconnect(client, 10000);
    MQTTClient_destroy(&client);
    return rc;
}