#This is a fork of the CCN-Lite project, created for the GreenIoT project at Uppsala University.#
**To start a router, run:** 

***./StartLite.sh -a start [PATH_TO_FOLDER_WITH_FILES_TO_SERVE]***

**The last argument is optional, use only if you have a folder with cached packets that you want to serve.**

When the router starts up, it sends a broadcast message to all available routers on the subnet that runs on port 9999 which is the default port for the application. 

For every router that responds, it creates a face to the IP of the responder.

Teamviewer Relay-B = 520 333 526, 7wv1c3