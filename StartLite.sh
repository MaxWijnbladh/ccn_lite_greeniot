#!/bin/bash
####################################
#
# Run CCN-Lite.
#
#	*NOTE* - path needs to be set for the ccn-lite folder for this script to work. (easy way is to add it to your ~/.profile file and restart the computer)
#	export CCNL_HOME="/path/to/your/installation/ccn_lite_greeniot"
#	export PATH=$PATH:"$CCNL_HOME/bin"
#
#	To give permissions (if needed):    chmod -x StartLite.sh   OR   sudo chmod 777 StartLite.sh
#	To run:                             ./StartLite

#	EXAMPLE for file on desktop. Write in terminal: 
#	chmod 777 ~/Desktop/StartLite.sh
#	$CCHL_HOME/StartLite.sh -h
#	$CCHL_HOME/StartLite.sh -s r2s1
#
####################################


############################
# Define needed Parameters #
############################

#variable for checking if any arguments were used
a_used=false
#variable for firefox loop (enables enter)
firefoxinfo=NULL
#variable for route y/n loop (enables enter)
createroutes=NULL



#############
# Functions #
#############

##-Help Function-##

helpfunction(){
	echo ""
	echo "     ###########################"
	echo "     ## HELP For StartLite.sh ##"
	echo "     ###########################"
	echo ""
	echo "-a [mode]    = Add a new relay | Arguments: simple , advanced"
	echo "               simple = define name and port"
	echo "               advanced = define: name, port, db path, cache size,"
	echo "                                  trace mode, http port"
	echo "               start = add relay named start on port 9999, ask for DB"
	echo ""
	echo "-c string    = Create content object | Arguments: string"
	echo ""
	echo "-f [name]    = Add face on relay | Arguments: *relay name*"
	echo ""
	echo "-i send      = Send an intrest | Arguments: send"
	echo ""
	echo "-k [who]     = Kill relays | Arguments: all , *relay name*"
	echo "               all        = Shutdown all the relays"
	echo "               relay name = shut down the relay"
	echo ""
	echo "-l [who]     = List relays | Arguments: all , *relay name*"
	echo "               all        = list all relay names"
	echo "               relay name = list all the info about the selected relay"
	echo ""
	echo "-r [name]    = Add prefix route on relay | Arguments: *relay name*"
	echo ""
	echo "-s [scene]   = Choose a scene to start | "
	echo "               [Default: NONE] | Arguments: r2s1 , loop"
	echo "               r2s1 = Opens 2 relays and 1 server. Creates route from"
	echo "                      RelayA->RelayB->serverC with knowledge for /ccn"
	echo "                      prefix. Intrest can be sent to port 9997 (RelayA)"
	echo "                      /ccn/test/mycontent is tried to fetch after start"
	echo "               loop = Opens 1 client, 4 relays, 1 server. Relays loop"
	echo "                      and cliend and server are on the opposite sides"
	echo "                      of the network."
	echo ""
	echo "   TODO:remove face or route"
	echo ""
}


##-Check if firefox status windows are wanted-##
needfirefox(){
	echo ""
	echo "Do you want to open relay info tabs on firefox? (works only on firefox)?(y/n)"
	while [[ $firefoxinfo != 'y' && $firefoxinfo != 'n' && $firefoxinfo != '' ]]; do
		read -n 1 firefoxinfo
	done
}

##-Check if pre-defined content prefixes are wanted-##
needroutes(){
	echo ""
	echo "Do you want to create prefix routes for the connections?(y/n)"
	while [[ $createroutes != 'y' && $createroutes != 'n' && $createroutes != '' ]]; do
		read -n 1 createroutes
	done
}



########################
# Check The Parameters #
########################

#Go trhough arguments
while getopts ":s:k:l:a:f:r:i:c:" opt; do
  case $opt in
    s) #ARGUMENT FOR SCENE
	   a_scene="$OPTARG"
	   a_used=true
    ;;
    k) #ARGUMENT FOR KILLING A RELAY PROCESS
	   a_kill="$OPTARG"
	   a_used=true
    ;;
    l) #ARGUMENT FOR LISTING A RELAY PROCESS
	   a_list="$OPTARG"
	   a_used=true
    ;;
    a) #ARGUMENT FOR LISTING A RELAY PROCESS
	   a_addrelay="$OPTARG"
	   a_used=true
    ;;
    f) #ARGUMENT FOR ADDING A FACE TO RELAY
	   a_addface="$OPTARG"
	   a_used=true
    ;;
    r) #ARGUMENT FOR ADDING A ROUTE TO RELAY
	   a_addroute="$OPTARG"
	   a_used=true
    ;;
    i) #ARGUMENT FOR INTREST
	   a_intrest="send"
	   a_used=true
    ;;
    c) #ARGUMENT FOR CREATING CONTENT
	   a_content="create"
	   a_used=true
    ;;
    \?) #BAD ARGUMENT, GET HELP
	   helpfunction >&2
    	exit 1
    ;;
    :) #ARGUMENT FOR ARGUMNTS WITH NO PARAMETERS
	  if [[ $OPTARG == "s" ]]; then
	  	echo "Option -$OPTARG arguments: r2s1"
	  elif [[ $OPTARG == "k" ]]; then
	  	echo "Option -$OPTARG arguments: all , *relay name*"
	  elif [[ $OPTARG == "l" ]]; then
	  	echo "Option -$OPTARG arguments: all , *relay name*"
	  elif [[ $OPTARG == "a" ]]; then
	  	echo "Option -$OPTARG arguments: simple , advanced , max"
	  elif [[ $OPTARG == "f" ]]; then
	  	echo "Option -$OPTARG arguments: *relay name*. [You can list relays by -l all]"
	  elif [[ $OPTARG == "r" ]]; then
	  	echo "Option -$OPTARG arguments: *relay name*. [You can list relays by -l all and get relay info by -l *relay name*]"
	  elif [[ $OPTARG == "i" ]]; then
	  	echo "Option -$OPTARG arguments: send"
	  elif [[ $OPTARG == "c" ]]; then
	  	echo "Option -$OPTARG arguments: create"
	  else
	  	echo "Option -$OPTARG requires an argument."
	  fi
      >&2
      exit 1
    ;;
  esac
done

#CHECK IF ANY ARGUMENTS WERE GIVEN IF NOT, GET HELP
if [[ $a_used = false ]]; then
	helpfunction
fi


# For Argument -c #
###############################
#       create content        #
###############################
if [[ "$a_content" != "" ]]; then
	# GIVE :Content path
	echo ""
	echo "(1 of 4) Give PATH to content store [DEFAULT: \$CCNL_HOME/test/ccntlv/]:"
	read contentpaht
	if [[ "$contentpaht" == "" ]]; then
		contentpaht="$CCNL_HOME/test/ccntlv/"
	fi
	# GIVE :Filename
	echo ""
	echo "(2 of 4) Give filename for the content [DEFAULT: mycontent]:"
	read contentname
	if [[ "$contentname" == "" ]]; then
		contentname="mycontent"
	fi
	# GIVE :Prefix
	echo ""
	echo "(3 of 4) Give prefix [DEFAULT: /ccn/test/mycontent]:"
	read prefix
	if [[ "$prefix" == "" ]]; then
		prefix="/ccn/test/mycontent"
	fi
	# GIVE :Content
	echo ""
	echo "(4 of 4) Give content message:"
	$CCNL_HOME/bin/ccn-lite-mkC -s ccnx2015 "$prefix" > $contentpaht$contentname.ccntlv
	#$CCNL_HOME/bin/ccn-lite-mkC -s ccnx2015 "/ccn/test/mycontent" > $CCNL_HOME/test/ccntlv/mycontent.ccntlv
fi


# For Argument -i #
#############################
#       send intrest        #
#############################
if [[ "$a_intrest" != "" ]]; then
	# GIVE :IP
	echo ""
	echo "(1 of 3) Give the destination IP (x.x.x.x) [\"local\" sets the local IP]:"
	read relayip
	while [[ "$relayip" == "" ]]; do
		read relayip
	done
	if [[ "$relayip" == "local" ]]; then
		relayip="127.0.0.1"
	fi
	# GIVE :Port
	echo ""
	echo "(2 of 3) Give the destination PORT (e.g.: 9999):"
	read relayport
	while [[ "$relayport" == "" ]]; do
		read relayport
	done
	# GIVE :Prefix
	echo ""
	echo "(3 of 3) Give prefix [DEFAULT: /ccn/test/mycontent]:"
	read prefix
	if [[ "$prefix" == "" ]]; then
		prefix="/ccn/test/mycontent"
	fi
	$CCNL_HOME/bin/ccn-lite-peek -s ccnx2015 -u $relayip/$relayport "$prefix" | $CCNL_HOME/bin/ccn-lite-pktdump -f 2
fi


# For Argument -a #
############################
#       add relay          #
############################


##SIMPLE
if [[ "$a_addrelay" == Start || "$a_addrelay" == start || "$a_addrelay" == START ]]; then


    # GIVE: DB path
	echo ""
	echo "(1 of 1) Need default folder to be added to cache? [DEFAULT:n](y/n)"
	read relayDBpath
	if [[ "$relayDBpath" == "y" || "$relayDBpath" == "Y" || "$relayDBpath" == "Yes" || "$relayDBpath" == "yes" || "$relayDBpath" == "YES" ]]; then
		relayDBpath=" -d $CCNL_HOME/test/ccntlv"
		echo "Database path set to: $relayDBpath"
	else
		relayDBpath=""
		echo "Nothing added to cache"
	fi

	gnome-terminal --title="Relay (max) port:9695" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 8080 -u 9695 -x /tmp/mgmt-relay-max.sock$relayDBpath" &

##SIMPLE
elif [[ "$a_addrelay" == Simple || "$a_addrelay" == simple || "$a_addrelay" == SIMPLE ]]; then
	# GIVE :Name
	echo "(1 of 2) Give a NAME for the relay:"
	read relayName
	while [[ "$relayName" == "" ]]; do
		read relayName
	done
	# GIVE :Port
	echo "(2 of 2) Give a PORT for the relay:"
	read relayPort
	if [[ "$relayPort" == "" ]]; then
		echo "Give a PORT for the relay:"
		read relayPort
	fi

	gnome-terminal --title="Relay ($relayName) port:$relayPort" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t $relayPort -u $relayPort -x /tmp/mgmt-relay-$relayName.sock" &

elif [[ "$a_addrelay" == Advanced || "$a_addrelay" == advanced || "$a_addrelay" == ADVANCED ]]; then
	# GIVE :Name
	echo ""
	echo "(1 of 6) Give a NAME for the relay:"
	read relayName
	while [[ "$relayName" == "" ]]; do
		read relayName
	done
	# GIVE :Port
	echo ""
	echo "(2 of 6) Give a PORT for the relay (e.g.: 9695):"
	read relayPort
	while [[ "$relayPort" == "" ]]; do
		read relayPort
	done
	# GIVE: DB path
	echo ""
	echo "(3 of 6) Give PATH for folder to be added to cache"
	echo "[ENTER sets default, \"NONE\" disables this option]:"
	read relayDBpath
	if [[ "$relayDBpath" == "" ]]; then
		relayDBpath=" -d $CCNL_HOME/test/ccntlv"
		echo "Database path set to: $relayDBpath"
	elif [[ "$relayDBpath" == "NONE" || "$relayDBpath" == "None" || "$relayDBpath" == "none" ]]; then
		relayDBpath=""
		echo "Pre-defined database on in use"
	else
		relayDBpath=" -d $relayDBpath"
		echo "Database path set to: $relayDBpath"
	fi
	# GIVE: Cache size
	echo ""
	echo "(4 of 6) Give CACHE SIZE in a number of stored interests"
	echo "[ENTER sets default (infinite), \"NONE\" disables this option]:"
	read relayCache
	if [[ "$relayCache" == "" ]]; then
		relayCache=""
		echo "cache set to default"
	elif [[ "$relayCache" == "NONE" || "$relayCache" == "None" || "$relayCache" == "none" ]]; then
		relayCache=" -c 0"
		echo "cache disabled"
	else
		relayCache=" -c $relayCache"
		echo "Database path set to: $relayCache"
	fi
	# GIVE: Debug
	echo ""
	echo "(5 of 6) Give DEBUG mode: fatal, error, warning, info, debug, verbose, trace"
	echo "[ENTER sets default (trace), \"NONE\" disables this option]:"
	read relayDebug
	while [[ "$relayDebug" != "" && "$relayDebug" != "NONE" && "$relayDebug" != "None" && "$relayDebug" != "none" && "$relayDebug" != "fatal" && "$relayDebug" != "error" && "$relayDebug" != "warning" && "$relayDebug" != "info" && "$relayDebug" != "debug" && "$relayDebug" != "verbose" && "$relayDebug" != "trace" ]]; do
		read relayDebug
	done
	if [[ "$relayDebug" == "" ]]; then
		relayDebug=" -v trace"
		echo "Trace set to default"
	elif [[ "$relayDebug" == "NONE" || "$relayDebug" == "None" || "$relayDebug" == "none" ]]; then
		relayDebug=""
		echo "Trace disabled"
	else
		relayDebug=" -v $relayDebug"
		echo "Trace set to: $relayDebug"
	fi
	# GIVE: HTTP
	echo ""
	echo "(6 of 6) Give HTTP port (e.g.: 9999) "
	echo "[ENTER sets http to relay port, \"NONE\" sets ccn-lite default (9695)]:"
	read relayHttp
	if [[ "$relayHttp" == "" ]]; then
		relayHttp=" -t $relayPort"
		echo "HTTP port set to: $relayPort"
	elif [[ "$relayHttp" == "NONE" || "$relayHttp" == "None" || "$relayHttp" == "none" ]]; then
		relayHttp=""
		echo "HTTP port set to: 9695"
	else
		relayHttp=" -v $relayHttp"
		echo "Trace set to: $relayHttp"
	fi
	#SUMMARY OF THE REALAY
	echo ""
	echo "--### RELAY SUMMARY ###--"
	echo "NAME:...... $relayName"
	echo "PORT:...... $relayPort"
	echo "DB PATH.... ${relayDBpath:4}"
	echo "CACHE...... ${relayCache:4}"
	echo "DEBUG...... ${relayDebug:4}"
	echo "HTTP....... ${relayHttp:4}"
	echo ""
	#START SERVER
	gnome-terminal --title="Relay ($relayName) port:$relayPort" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay$relayDebug$relayCache -s ccnx2015$relayHttp -u $relayPort -x /tmp/mgmt-relay-$relayName.sock$relayDBpath" &
elif [[ "$a_addrelay" != "" ]]; then
	echo "ERROR: invalid argument for -a. Arguments: simple , advanced , max"
fi


# For Argument -f #
#########################
#       add face        #
#########################
if [[ "$a_addface" != "" ]]; then
	# GIVE :IP
	echo ""
	echo "(1 of 2) Give the destination IP (x.x.x.x) [\"local\" sets the local IP]:"
	read relayip
	while [[ "$relayip" == "" ]]; do
		read relayip
	done
	if [[ "$relayip" == "local" ]]; then
		relayip="127.0.0.1"
	fi
	# GIVE :Port
	echo ""
	echo "(2 of 2) Give the destination PORT (e.g.: 9999):"
	read relayport
	while [[ "$relayport" == "" ]]; do
		read relayport
	done
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-$a_addface.sock newUDPface any $relayip $relayport | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
fi


# For Argument -r #
#########################
#       add route        #
#########################
if [[ "$a_addroute" != "" ]]; then
	# GIVE :FACEID
	echo ""
	echo "(1 of 2) Give the FACEID where to add route [ENTER adds to the lastest created face]:"
	read relayfaceid
	while [[ "$relayfaceid" == "" ]]; do
		read relayfaceid
	done
	# GIVE :PREFIX
	echo ""
	echo "(2 of 2) Give the PREFIX for the route [ENTER adds default /ccn]:"
	read relayprefix
	if [[ "$relayprefix" == "" ]]; then
		relayprefix="/ccn"
	fi
	if [[ "$relayfaceid" == "" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-$a_addroute.sock prefixreg $relayprefix $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
	fi
	$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-$a_addroute.sock prefixreg $relayprefix $relayfaceid ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
fi


# For Argument -l #
############################
#       list relays        #
############################
if [[ "$a_list" == "all" ]]; then
	echo ""
	echo "##############"
	echo "# ALL FACES: #"
	echo "##############"
	for x in /tmp/*.sock; do 
	  if [[ $x == "/tmp/*.sock" ]]; then
	  	echo "No faces found"
	  	exit 1
	  fi
	  echo $x | rev | cut -d '-' -f1 | rev |cut -d '.' -f1
	done
elif [[ "$a_list" != "" ]]; then
	echo ""
	echo "##############"
	echo "# FACE INFO: #"
	echo "##############"
	for x in /tmp/*.sock; do 
	  if [[ $x == "/tmp/*.sock" ]]; then
	  	echo "No faces found"
	  	exit 1
	  elif [[ "$x" == *$a_list\.sock* ]]; then
	    #echo $x | grep "$a_list.sock" | rev | cut -d '-' -f1 | rev |cut -d '.' -f1
	    $CCNL_HOME/bin/ccn-lite-ctrl -x $x debug dump | $CCNL_HOME/bin/ccn-lite-ccnb2xml
	    exit 1
	  fi
	done
	echo "Nothing found by \"$a_list\"!"
fi


# For Argument -k #
####################################
#       Kill all the relays        #
####################################
if [[ "$a_kill" == "all" ]]; then
	for x in /tmp/*.sock; do 
	  if [[ $x == "/tmp/*.sock" ]]; then
	  	exit 1
	  fi
	  $CCNL_HOME/bin/ccn-lite-ctrl -x $x debug halt | $CCNL_HOME/bin/ccn-lite-ccnb2xml &
	done
elif [[ "$a_kill" != "" ]]; then
	for x in /tmp/*.sock; do 
	  if [[ $x == "/tmp/*.sock" ]]; then
	  	exit 1
	  elif [[ "$x" == *$a_kill\.sock* ]]; then
	  	echo "REMOVING RELAY $a_kill"
	  	$CCNL_HOME/bin/ccn-lite-ctrl -x $x debug halt | $CCNL_HOME/bin/ccn-lite-ccnb2xml &
	  	exit 1
	  fi
	done
	echo "No relays found by \"$a_kill\"! NOTHING GOT REMOVED."
fi


# For Argument -s #
####################################
# Start test scenario for 3 relays #
####################################
if [[ $a_scene == "r2s1" ]]; then

	#Get Questions
	needfirefox

	echo ""
	echo "Enter the message for the /ccn/test/mycontent test file that is fetched after satrting the services (Hit Enter or Return when done):"
	$CCNL_HOME/bin/ccn-lite-mkC -s ccnx2015 "/ccn/test/mycontent" > $CCNL_HOME/test/ccntlv/mycontent.ccntlv

	#Creating three relays a ,b and c
	gnome-terminal --title="Relay (A) port:9997" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9997 -u 9997 -x /tmp/mgmt-relay-a.sock" &
	sleep 0.2
	#
	gnome-terminal --title="Relay (B) port:9998" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9998 -u 9998 -x /tmp/mgmt-relay-b.sock" &
	sleep 0.2
	#
	gnome-terminal --title="Relay/Server (C) port:9999" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9999 -u 9999 -x /tmp/mgmt-relay-c.sock -d $CCNL_HOME/test/ccntlv" &
	sleep 0.5

	#CONFIGURE RELAY B TO KNOW WHERE C IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-b.sock newUDPface any 127.0.0.1 9999 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-b.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
	sleep 0.2

	#CONFIGURE RELAY A TO KNOW WHERE B IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-a.sock newUDPface any 127.0.0.1 9998 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-a.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
	sleep 0.2

	#OPEN HTTP IF WANTED
	if [[ $firefoxinfo == "y" ]]; then
		nohup firefox http://127.0.0.1:9997 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9997 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9998 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9999 &
	fi

	echo ""
	echo "Relay configuration ready!"
	echo ""
	echo "Fetching the test content:"
	sleep 0.5
	$CCNL_HOME/bin/ccn-lite-peek -s ccnx2015 -u 127.0.0.1/9997 "/ccn/test/mycontent" | $CCNL_HOME/bin/ccn-lite-pktdump -f 2

fi

# For Argument -s #
################################################################
# Start test scenario for 4 relays 1 server 1 "client gateway" #
################################################################
if [[ $a_scene == "loop" ]]; then

	#Get Questions
	needfirefox
	needroutes

	echo ""
	echo "Enter the message for the /ccn/test/mycontent test file that is fetched after satrting the services (Hit Enter or Return when done):"
	$CCNL_HOME/bin/ccn-lite-mkC -s ccnx2015 "/ccn/test/mycontent" > $CCNL_HOME/test/ccntlv/mycontent.ccntlv

	#Creating three relays server, a, b, c, d, client
	echo "LOADING: Creating relays"
	#
	gnome-terminal --title="Relay (server) port:9994" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9994 -u 9994 -x /tmp/mgmt-relay-server.sock -d $CCNL_HOME/test/ccntlv" &
	sleep 0.2
	#
	gnome-terminal --title="Relay (d) port:9995" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9995 -u 9995 -x /tmp/mgmt-relay-d.sock" &
	sleep 0.2
	#
	gnome-terminal --title="Relay (c) port:9996" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9996 -u 9996 -x /tmp/mgmt-relay-c.sock" &
	sleep 0.2
	#
	gnome-terminal --title="Relay (b) port:9997" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9997 -u 9997 -x /tmp/mgmt-relay-b.sock" &
	sleep 0.2
	#
	gnome-terminal --title="Relay (a) port:9998" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9998 -u 9998 -x /tmp/mgmt-relay-a.sock" &
	sleep 0.2
	#
	gnome-terminal --title="Relay/Server (client) port:9999" -x sh -c "!!; cd $CCNL_HOME/src/; pwd ; $CCNL_HOME/bin/ccn-lite-relay -v trace -s ccnx2015 -t 9999 -u 9999 -x /tmp/mgmt-relay-client.sock" &
	sleep 0.2
	#
	echo "DONE: Creating relays"
	echo "LOADING: Creating faces"
	sleep 0.5

	#CONFIGURE RELAY CLIENT TO KNOW WHERE A IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-client.sock newUDPface any 127.0.0.1 9998 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	if [[ $createroutes == "y" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-client.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
		echo "-DONE: prefix route c->server"
	fi
	echo "-DONE: connection client->a"
	sleep 0.2

	#CONFIGURE RELAY A TO KNOW WHERE B IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-a.sock newUDPface any 127.0.0.1 9997 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	if [[ $createroutes == "y" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-a.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
		echo "-DONE: prefix route c->server"
	fi
	echo "-DONE: connection a->b"
	sleep 0.2

	#CONFIGURE RELAY A TO KNOW WHERE D IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-a.sock newUDPface any 127.0.0.1 9995 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	if [[ $createroutes == "y" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-a.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
		echo "-DONE: prefix route c->server"
	fi
	echo "-DONE: connection a->d"
	sleep 0.2

	#CONFIGURE RELAY B TO KNOW WHERE C IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-b.sock newUDPface any 127.0.0.1 9996 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	if [[ $createroutes == "y" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-b.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
		echo "-DONE: prefix route c->server"
	fi
	echo "-DONE: connection b->c"
	sleep 0.2

	#CONFIGURE RELAY D TO KNOW WHERE C IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-d.sock newUDPface any 127.0.0.1 9996 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	if [[ $createroutes == "y" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-d.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
		echo "-DONE: prefix route c->server"
	fi
	echo "-DONE: connection d->c"
	sleep 0.2

	#CONFIGURE RELAY C TO KNOW WHERE SERVER IS
	FACEID=`$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-c.sock newUDPface any 127.0.0.1 9994 | $CCNL_HOME/bin/ccn-lite-ccnb2xml | grep FACEID | sed -e 's/^[^0-9]*\([0-9]\+\).*/\1/'`
	sleep 0.2
	if [[ $createroutes == "y" ]]; then
		$CCNL_HOME/bin/ccn-lite-ctrl -x /tmp/mgmt-relay-c.sock prefixreg /ccn $FACEID ccnx2015 | $CCNL_HOME/bin/ccn-lite-ccnb2xml
		echo "-DONE: prefix route c->server"
	fi
	echo "-DONE: connection c->server"
	sleep 0.2

	#OPEN HTTP IF WANTED
	if [[ $firefoxinfo == "y" ]]; then
		nohup firefox http://127.0.0.1:9994 &
		sleep 0.5
		nohup firefox --new-window http://127.0.0.1:9994 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9995 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9996 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9997 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9998 &
		sleep 0.5
		nohup firefox --new-tab http://127.0.0.1:9999 &
	fi

	echo ""
	echo "DONE: Relay configuration ready!"
	echo ""
	echo "#-SETUP:-#"
	echo ""
	echo " server"
	echo "   |  "
	echo "   c"
	echo "  / \ "
	echo " b   d"
	echo "  \ /"
	echo "   a"
	echo "   | "
	echo " client"
	echo "(client knows about a, a knows about b and d, b nad d knows about c, c knows about server)"
	echo ""
	echo "Client fetching the test content form server:"
	sleep 0.1
	$CCNL_HOME/bin/ccn-lite-peek -s ccnx2015 -u 127.0.0.1/9999 "/ccn/test/mycontent" | $CCNL_HOME/bin/ccn-lite-pktdump -f 2

fi