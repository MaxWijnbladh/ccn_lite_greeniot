from pymongo import MongoClient
import csv, json, StringIO, ast, sys, socket, threading, os, ConfigParser, time, datetime
from urllib2 import urlopen
import os.path
from subprocess import call
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 15, 6
from statsmodels.tsa.stattools import adfuller, acf,pacf
from statsmodels.tsa.arima_model import ARIMA

global IDENTITY_TXT
global IDENTITY_PACKET
global PREDICTION_LIST_TXT
global PREDICTION_LIST_PACKET
global PREDICTION_LIST_PREFIX_FOO
global PREDICTION_LIST_PREFIX_MO
global PREDICTION_LIST_PREFIX_RULL
global HISTORIC_PREFIX_F00
global HISTORIC_PREFIX_MO
global HISTORIC_PREFIX_RULL
global DATABASE_IP
global DATABASE_PORT
global SPECIFIC_TXT
global SPECIFIC_PACKET

def getConfig():

	print "Reading config file..."
	Config = ConfigParser.ConfigParser()
	Config.read('nfnconfig.ini')

	
	global IDENTITY_PACKET
	global IDENTITY_TXT
	global SENSOR_LIST_TXT
	global PREDICTION_LIST_TXT
	global PREDICTION_LIST_PACKET
	global HISTORICAL_LIST_TXT
	global HISTORICAL_LIST_PACKET
	global DATABASE_IP
	global DATABASE_PORT
	global SENSOR_LIST_PREFIX
	global CCN_SUITE
	global SDS_IP
	global DATABASE_IP
	global DATABASE_PORT
	global SDS_PORT
	global LOCALHOST_IP
	global LOCALHOST_PORT
	global SPECIFIC_TXT
	global SPECIFIC_PACKET

	SPECIFIC_TXT = Config.get("VALUES","SPECIFIC")
	SPECIFIC_PACKET = Config.get("VALUES", "SPECIFIC_PACKET")
	IDENTITY_TXT = Config.get("IDENTITY", 'IDENTITY_TXT')	
	IDENTITY_PACKET = Config.get("IDENTITY", 'IDENTITY_PACKET')
	PREDICTION_LIST_TXT = Config.get("VALUES", 'PREDICTION')
	PREDICTION_LIST_PACKET = Config.get("VALUES", 'PREDICTION_PACKET')
	DATABASE_IP = Config.get('DATABASE', 'DATABASE_IP') 
	DATABASE_PORT = Config.get("DATABASE", 'DATABASE_PORT')
	HISTORICAL_LIST_TXT = Config.get("VALUES", 'HISTORICAL')
	HISTORICAL_LIST_PACKET = Config.get("VALUES", 'HISTORICAL_PACKET')

def dbConfig():
	global DATABASE
	client = MongoClient(DATABASE_IP+":"+DATABASE_PORT, serverSelectionTimeoutMS=3000)
	try:
		client.server_info()
	except:
		print "Couldn't connect to sensor database, exiting"
		sys.exit()
	print "Connected to database! ("+DATABASE_IP+":"+DATABASE_PORT+")"
	DATABASE = client.test.sensors

def udp_socket_init():
    UDP_IP = ""
    UDP_PORT = 9997
    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    return sock

def send_udp(sensorList, sender_ip, sock):
	numBytes = sock.sendto(sensorList, sender_ip)	
	return numBytes

def send_identity(sock, sender_ip): 
	#create sds discovery content packet
	buf = 1024
	identity_txt = open(IDENTITY_TXT, 'w')
	identity_txt.write('130.238.15.214')
	identity_txt.close()
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + IDENTITY_TXT + " -s " + "ndn2013" + " -o " + IDENTITY_PACKET + " " + "/nfn")
	#send it to local relay
	f = open(IDENTITY_PACKET,"rb")
	if f:
		data = f.read(buf)
		f.close()
	print "Sending identity packet..."
	numBytes = send_udp(data, sender_ip, sock) 
	print "Identity packet sent! (" + str(numBytes) + " bytes)"
	print "Prediction initialized!"


def prediction():
	data=pd.read_csv("prediction.csv")

	dateparse = lambda dates: pd.datetime.strptime(dates, '%Y-%m-%d %H:%M:%S')
	data = pd.read_csv("prediction.csv", parse_dates='Date', index_col='Date',date_parser=dateparse)

	print data["Values"]
	ts = data["Values"] 

	ts = pd.Series(ts)

	ts_log = np.log(ts)

	ts_log_diff = ts_log - ts_log.shift()

	model = ARIMA(ts_log, order=(2, 1, 0))
	results_AR = model.fit(disp=-1)  
	plt.plot(ts_log_diff)
	plt.plot(results_AR.fittedvalues, color='red')
	plt.title('RSS: %.4f'% sum((results_AR.fittedvalues-ts_log_diff)**2))
	predictions_ARIMA_diff=pd.Series(results_AR.fittedvalues,copy=True)
	predictions_ARIMA_diff_cumsum=predictions_ARIMA_diff.cumsum()
	predictions_ARIMA_log = pd.Series(ts_log.ix[0], index=ts_log.index)
	predictions_ARIMA_log = predictions_ARIMA_log.add(predictions_ARIMA_diff_cumsum,fill_value=0)
	predictions_ARIMA_log.head()
	#predicted = predictions_ARIMA_log.head(6)
	predictions_ARIMA = np.exp(predictions_ARIMA_log.head(24))
	predicted = predictions_ARIMA
	#plt.plot(ts)
	#plt.plot(predictions_ARIMA)
	#plt.title('RMSE: %.4f'% np.sqrt(sum((predictions_ARIMA-ts)**2)/len(ts)))
	#plt.show()
	print predicted[0]
	print type(predicted)

	f = open(PREDICTION_LIST_TXT,"w") 
	f.write(str(predicted))
	f.close()

def createContent(prefix, filename, packet): 
	print "Creating content packet..."
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + filename + " -s " + "ndn2013" + " -o " + packet + " " + prefix)
	print "Content packet created!"

def getPreData(interest):
	splittedinterest = interest.split("/")
	location = splittedinterest[0]
	with open("prediction.csv", 'w') as csvfile:
		    spamwriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
		    spamwriter.writerow(["Date", "Values"])
		    for s in DATABASE.find():
		    	if location in s["pf"]:
		    		for i in range(0,len(s["e"])-1):
		    			e = s["e"][i]
		    			if(i > 2):
		    				e = ast.literal_eval(s["e"][i])
		    			timeInSeconds = e["t"]
		    			currentDate = datetime.datetime.fromtimestamp(timeInSeconds).strftime('%Y-%m-%d %H:%M:%S')
		    			if(e["n"] == "Humidity"):
		    				curr = float(e["v"])
		    				if(curr != 0):
		    					print curr
		    					spamwriter.writerow([currentDate, curr])



def historicVal(interest, N):
	splittedinterest = interest.split("/")
	location = splittedinterest[0]
	print location
	values = []
	for data in DATABASE.find():
		if location in data["pf"]:
			#GRINGO IS A PUTA CHANGE HERE
			for i in range(len(data["e"]),len(data["e"])-24, -1):
				#print i
				try:
					e = data["e"][i]
					values.append(e)
				except:
					continue
	print values
	f = open(HISTORICAL_LIST_TXT,"w") 
	f.write(str(values))
	f.close()


def get_spec_data(interest):
	singleVal = []
	splittedinterest = interest.split("/")
	print splittedinterest
	location = splittedinterest[2]
	mac = splittedinterest[3]
	seqnr = splittedinterest[4]
	print location
	print seqnr
	for data in DATABASE.find():
		if (location and mac) in data["pf"]:
			for i in range(0,len(data["e"])):
		    			e = data["e"][i]
		    			if(i > 2):
		    				e = ast.literal_eval(data["e"][i])
		    			if ((e["n"] == "Humidity") and (e["sn"] == seqnr)):
		    				singleVal.append(e["v"])
	print singleVal
	if(len(singleVal) != 0):
		f = open(SPECIFIC_TXT,"w") 
		f.write(str(singleVal))
		f.close()

def send_interest():
	print "Sending interests..."
	process = os.popen("$CCNL_HOME/bin/ccn-lite-peek -s ndn2013 -u 127.0.0.1/9695 '/sds/namedfunction/pre'  | $CCNL_HOME/bin/ccn-lite-pktdump")
	process = os.popen("$CCNL_HOME/bin/ccn-lite-peek -s ndn2013 -u 127.0.0.1/9695 '/sds/namedfunction/his'  | $CCNL_HOME/bin/ccn-lite-pktdump")
	print "Interests sent!"
	

def getCharIndexes(s, ch):
    return [i for i, ltr in enumerate(s) if ltr == ch]

def get_prefix_from_interest(interest):
	res = ""
	indexes = getCharIndexes(interest, "\n")
	curr = 0
	for i in interest:
		if(i == "\n" and curr == indexes[-1]):
			break
		if(i.isdigit() or i.isalpha()):
			res +=i
		else:
			res+="/"
		curr+=1
	res = list(res)
	for i in range(0, len(res)-1):
		if(res[i] == "/" and res[i] == res[i+1]):
			res[i] = "-"
	res = "".join(res).replace("-","")
	print res
	return res

def main():

	UDP_PORT = 9997
	buf = 1024
	sender_ip = None

	getConfig()
	dbConfig()


	sock = udp_socket_init()
	print "Listening socket opened! (127.0.0.1:"+str(UDP_PORT)+")"
	#send path to SDS to local relay
	send_identity(sock, ("0.0.0.0", 9695))
	#send_interest()

	print "#######################################################################\n"
	print "               Prediction program\n"
	print "                           /\___/\ "
	print "                          (--o-o--) "
	print "                          /   *   \ "
	print "                          \__\_/__/ I'm Predictiondrew, meow!"
	print "                            /   \ "
	print "                           / ___ \ "
	print "                           \/___\/"
	print "#######################################################################"

	while True:

		#Wait for packet 
		try:
			os.remove(SPECIFIC_TXT)
		except:
			pass
		data, sender_ip = sock.recvfrom(1024) # buffer size is 1024 bytes
		print "-----------------------------------------------------------------------"
		print "Waiting for packet..."
		print "Recieved packet..."	
		interest = get_prefix_from_interest(data)
		if "sds" in interest:
			continue
		if "/nfn/his"  in interest:
			print "hist"
			N = 24
			#N = latest N values from db
			historicVal(interest, N)
			createContent(interest, HISTORICAL_LIST_TXT, HISTORICAL_LIST_PACKET)
			o = open(HISTORICAL_LIST_PACKET,"rb")
			if o:
				data = o.read(buf)
				o.close()
				#send data over udp
				print "Sending content packet..."
				print sender_ip
				numBytes = send_udp(data, sender_ip, sock)
				print "Content packet sent , " + str(numBytes) + " bytes!"
			else:
				print "Error loading content object, abort"
				continue
		elif "/nfn/pre" in str(interest):
			getPreData(interest)
			prediction()
			createContent(interest, PREDICTION_LIST_TXT, PREDICTION_LIST_PACKET)
			o = open(PREDICTION_LIST_PACKET,"rb")
			if o:
				data = o.read(buf)
				o.close()
				#send data over udp
				print "Sending content packet..."
				print sender_ip
				numBytes = send_udp(data, sender_ip, sock)
				print "Content packet sent , " + str(numBytes) + " bytes!"
			else:
				print "Error loading content object, abort"
				continue
		elif "/p/" in str(interest):
			print "other"
			get_spec_data(interest)
			if(os.path.exists(SPECIFIC_TXT)):
				createContent(interest, SPECIFIC_TXT, SPECIFIC_PACKET)
				o = open(SPECIFIC_PACKET, "rb")
				if o:
					data = o.read(buf)
					o.close()
					#send data over udp
					print "Sending content packet..."
					print sender_ip
					numBytes = send_udp(data, sender_ip, sock)
					print "Content packet sent , " + str(numBytes) + " bytes!"
				else:
					print "Error loading content object, abort"
					continue

if __name__ == "__main__":
    main()

