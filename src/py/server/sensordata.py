import json, time, os, subprocess, StringIO, sys, ConfigParser, math, threading, socket
from pymongo import MongoClient
from bson.json_util import dumps
from urllib2 import urlopen
import paho.mqtt.client as mqtt


class sensorThread(threading.Thread):
    def __init__(self, prefix, looptime, time, sensor_mac, location):
        threading.Thread.__init__(self)
        self._stopevent = threading.Event()
        self.prefix = prefix
        self.looptime = looptime
        self.time = time
        self.sensor_mac = sensor_mac
        self.location = location
    def run(self):
    	while not self._stopevent.isSet():
	        print "Sensor " + self.prefix
	        beforeTime = time.time()
	        seqno = calcSeq(self.prefix, self.looptime, self.time)
	        peekSensors(self.prefix, seqno, self.time, self.sensor_mac, self.location, self.looptime)
	        afterTime = time.time()
	        diffTime = afterTime - beforeTime
	        time.sleep(int(self.looptime) - diffTime)

class listenerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self._stopevent = threading.Event()
    def run(self):
    	sock = udp_socket_init()
    	print "sock listening"
    	while not self._stopevent.isSet() or not exit:
    		data, sender_ip = sock.recvfrom(1024)
    		print "data"
    		print data
    		print "sender_ip"
    		print sender_ip
    		for t in threads:
    			t._stopevent.set()
    		start_threads()




def udp_socket_init():
    UDP_IP = ""
    UDP_PORT = 9999
    sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))
    return sock

def getConfig():


	print "Reading config file..."
	Config = ConfigParser.ConfigParser()
	Config.read('sensorConfig.ini')

	global SENSOR_LIST_PREFIX
	global CCN_SUITE

	global DATABASE_IP
	global DATABASE_PORT

	global LOCALHOST_IP
	global LOCALHOST_PORT




	SENSOR_LIST_PREFIX = Config.get("SENSORS", 'SENSOR_LIST_PREFIX')
	CCN_SUITE = Config.get("CCN", 'CCN_SUITE')
	DATABASE_IP = Config.get('DATABASE', 'DATABASE_IP') 
	DATABASE_PORT = Config.get("DATABASE", 'DATABASE_PORT')
	LOCALHOST_IP = Config.get("LOCALHOST", 'LOCALHOST_IP')
	LOCALHOST_PORT = Config.get("LOCALHOST", 'LOCALHOST_PORT')

def getSensorLists():
	sensor_array = []

	for sensor in DATABASE.find():
		if sensor['a'] == True:
			location = sensor['lo']
			prefix = sensor['pf']
			sensorMac = sensor['bn']
			time = sensor['bt']
			loopTime = sensor['but']
			sensor_array.append((prefix, loopTime, time, sensorMac, location))

	return sensor_array

def dbSetup():
	global DATABASE
	client = MongoClient(DATABASE_IP+":"+DATABASE_PORT, serverSelectionTimeoutMS=3000)
	try:
		client.server_info()
	except:
		print "Couldn't connect to sensor database, exiting"
		sys.exit()
	print "Connected to database! ("+DATABASE_IP+":"+DATABASE_PORT+")"
	DATABASE = client.test.sensors

def mqttconnect():
	global mqttclient
	mqttclient = mqtt.Client()
	mqttclient.connect("mqtt.greeniot.it.uu.se", 1883, 60)

def calcSeq(prefix, looptime, initialTime):
	nowTime = time.time()
	diffTime = nowTime - initialTime
	currentSeq = int(diffTime / (int(looptime))) - 2
	if(currentSeq % 15 == 0):
		currentSeq -= 1
	currentSeq = currentSeq % 1800
	cursor = DATABASE.find({"pf": prefix})
	for prefix in cursor:
		DATABASE.update_one({'pf': prefix["pf"]},{'$set': {'bsn': str(currentSeq)}}, upsert=False)
	print currentSeq
	return currentSeq


def peekSensors(prefix, seqno, sensor_time, sensor_mac, location, looptime):

	process = os.popen("$CCNL_HOME/bin/ccn-lite-peek -s " + CCN_SUITE + " -u " + LOCALHOST_IP + "/" + LOCALHOST_PORT + " " + prefix + "/" + str(seqno) + " | $CCNL_HOME/bin/ccn-lite-pktdump -f 2")
	sensor_result = process.read()
	print sensor_result	
	if(len(sensor_result) > 0):
			#search if in  DB
		tokens = sensor_result.split("-")
		light = tokens[2]
		temp = tokens[3]
		humidity = tokens[4]
		cursor = DATABASE.find({"pf": prefix + "/" + str(seqno)})			
		if cursor.count()==0:
				#content object not in DB
			senMLres = {"n": "Temperature","u": "C","t": time.time(),"v": temp,"sn": seqno}, {"n": "Light","u": "Lu","t": time.time(),"v": light,"sn": seqno}, {"n": "Humidity","u": "%","t": time.time(),"v": humidity,"sn": seqno}
			

			Lvalue = {}
			Lvalue["n"] = "Light"
			Lvalue["u"] = "Lux"
			Lvalue["v"] = light
			Lvalue["t"] = int(time.time())
			Lvalue["sn"] = seqno

			Tvalue = {}
			Tvalue["n"] = "Temperature"
			Tvalue["u"] = "C"
			Tvalue["v"] = temp
			Tvalue["t"] = int(time.time())
			Tvalue["sn"] = seqno

			Hvalue = {}
			Hvalue["n"] = "Humidity"
			Hvalue["u"] = "%"
			Hvalue["v"] = humidity
			Hvalue["t"] = int(time.time())
			Hvalue["sn"] = seqno

			result = DATABASE.update({'pf': prefix}, {'$push': { 'e': str(Lvalue)}}, upsert=False)
			result = DATABASE.update({'pf': prefix}, {'$push': { 'e': str(Tvalue)}}, upsert=False)
			result = DATABASE.update({'pf': prefix}, {'$push': { 'e': str(Hvalue)}}, upsert=False)
			
			#mqttclient.publish(prefix+ "/" + str(seqno), str(json.dumps(senMLres)), 1)

			print result
		else:
			print "***Error content object already in DB"
	else:
		print("***Error NO SENSOR RESULT")

def start_threads():
	sensor_array = getSensorLists()
	if (sensor_array):
		for sensor in sensor_array:
			t = sensorThread(sensor[0], sensor[1], sensor[2], sensor[3], sensor[4])
			t.start()
			threads.append(t)				

def main():
	global threads
	global listerners
	getConfig()
	#mqttconnect()
	#print "mqtt conn"
	dbSetup()
	sensor_array = getSensorLists()
	threads = []
	listerners = []
	t = listenerThread()
	t.start()
	listerners.append(t)
	start_threads()


if __name__ == "__main__":
	main()
