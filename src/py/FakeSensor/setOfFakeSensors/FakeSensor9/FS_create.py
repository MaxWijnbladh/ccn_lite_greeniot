######################################################
#- FAKE SENSOR - GENERATE STUFF FOR CONTENT OBJECTS -#
######################################################
#
import random, sys, time, json, socket, os, shutil, signal
#
##########################
###- Give parameters: -###
##########################
#[DATA]
mac_address = "00:12:45:fa:ke:09"
LOCATION    = "Virtu1"
LOCATIONFULL= "Virtual Set Of Sensors 1"
UPDATE_TIME = 10

CCN_SUITE   = "ndn2013"
CCN_RELAY_IP = "130.238.15.200"
CCN_FACE_PORT = "9988"

#[UDP to SDS]
UDP_IP      ="130.238.15.194"
UDP_PORT    = 9999
#########################

###################
###- FUNCTIONS -###
###################


def find_data_folder():
	if not os.path.exists("data/"):
		os.makedirs("data")

def signal_handler(signal, frame):
	print "\n----------------------------------------------------------------------"
	print('FAKE SENSOR SHUTDOWN!')
	print('Removing session data...')
	#remove old data
	folder = 'data/'
	for the_file in os.listdir(folder):
	    file_path = os.path.join(folder, the_file)
	    try:
	        if os.path.isfile(file_path):
	            os.unlink(file_path)
	        #elif os.path.isdir(file_path): shutil.rmtree(file_path)
	    except Exception as e:
	        print(e)
	print('-GOOD BYE-')
	print "-----------------------------------------------------------------------"
	sys.exit(0)

#########################
###- Automatic stuff -###
#########################
find_data_folder()
signal.signal(signal.SIGINT, signal_handler)
mac_address_prefix = mac_address.translate(None, ':')

try:
				
		#Create new ipv6 face to new sensor
	face_id = os.popen('$CCNL_HOME/bin/ccn-lite-ctrl -u '+CCN_RELAY_IP+' newUDPface any '+CCN_RELAY_IP+' '+CCN_FACE_PORT+' | $CCNL_HOME/bin/ccn-lite-ccnb2xml').read()
		#Take out the face ID from teh face_id. (Oh goosh! It looks hidious!)
	face_id = face_id.split('FACEID>')
	face_id = face_id[1].split('<')
	face_id = face_id[0]


		#Prefix routes to the sensor values for relay
	prefix = "/p/"+LOCATION+"/"+mac_address_prefix[-8:]
	#prefix = "/p/"+LOCATION
	os.system('$CCNL_HOME/bin/ccn-lite-ctrl -u '+CCN_RELAY_IP+' prefixreg '+prefix+' '+face_id+' '+CCN_SUITE+' | $CCNL_HOME/bin/ccn-lite-ccnb2xml')
except:
	print "Failed to create face"

light       = float("{0:.2f}".format(random.uniform(58.1, 60.7)))
temperature = float("{0:.2f}".format(random.uniform(21.5, 27.7)))
humidity    = float("{0:.2f}".format(random.uniform(12.0, 14.3)))

content = "TI CC26xx SensorTag-"+str(UPDATE_TIME)+"-"+str(light)+"-"+str(temperature)+"-"+str(humidity)+"-"
jasonMessageTemp = []




content = content.split('-')
data = {}
data["bn"] = mac_address
data["a"] = True
data["bt"] = time.time()
data["pf"] = "/p/"+LOCATION+"/"+mac_address_prefix[-8:]
data["lo"] = LOCATIONFULL
data["md"] = content[0]
data["but"] = content[1]
data["bsn"] = "1"
data["ver"] = "1"

Lvalue = {}
Lvalue["n"] = "Light"
Lvalue["u"] = "lx"
Lvalue["v"] = content[2]
Lvalue["t"] = time.time()
Lvalue["sn"] = "1"

Tvalue = {}
Tvalue["n"] = "Temperature"
Tvalue["u"] = "C"
Tvalue["v"] = content[3]
Tvalue["t"] = time.time()
Tvalue["sn"] = "1"

Hvalue = {}
Hvalue["n"] = "Humidity"
Hvalue["u"] = "%"
Hvalue["v"] = content[4]
Hvalue["t"] = time.time()
Hvalue["sn"] = "1"

data["e"] = [Tvalue,Lvalue,Hvalue]

print "\n#- Sensor INFO created -#"


jasonMessageTemp.append(data)

	#Create a json object from the data
json_data = json.dumps(jasonMessageTemp)
MESSAGE = json_data
		
	#Send UDP message
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

print "#- Sensor INFO sent -#"


content = "1-"+str(UPDATE_TIME)+"-"+str(light)+"-"+str(temperature)+"-"+str(humidity)+"-"
refresh_file = open('data/1.txt', 'w+')
print>>refresh_file, content
refresh_file.close()
print "#- Sensor Content 1 created -#"
sqn = 2

time.sleep(UPDATE_TIME)

while 1:
	light       = float("{0:.2f}".format(random.uniform(58.1, 60.7)))
	temperature = float("{0:.2f}".format(random.uniform(21.5, 27.7)))
	humidity    = float("{0:.2f}".format(random.uniform(12.0, 14.3)))

	content = str(sqn)+"-"+str(UPDATE_TIME)+"-"+str(light)+"-"+str(temperature)+"-"+str(humidity)+"-"
	filename = "data/"+str(sqn)+".txt"

	refresh_file = open(filename, 'w+')
	print>>refresh_file, content
	refresh_file.close()
	print "#- Sensor Content "+str(sqn)+" created -#"

	if sqn > 14:
		try:
			os.system('rm data/'+str(sqn-14)+'.txt')
		except:
			pass

	time.sleep(UPDATE_TIME)
	sqn = (sqn+1)%1800
