######################################################
#- FAKE SENSOR - GENERATE STUFF FOR CONTENT OBJECTS -#
######################################################
#
import socket, sys, signal, json, os
from os import listdir
from os.path import isfile, join
#
global SOCKET
##########################
###- Give parameters: -###
##########################
#[DATA]
mac_address = "00:12:45:fa:ke:07"
LOCATION    = "Virtu1"
fakecontent = "fakecontent1"

#[UDP to SDS for inactive message]
UDP_IP_SDS      ="130.238.15.194"
UDP_PORT_SDS    = 9999

#[UDP from Relay to listen]
UDP_IP      =""
UDP_PORT    = 9986
#########################

#########################
###- Automatic stuff -###
#########################
mac_address_prefix = mac_address.translate(None, ':')



###################
###- Functions -###
###################
def find_data_folder():
	if not os.path.exists("data/"):
		os.makedirs("data")


def udp_socket_init(udp_ip,udp_port):
	global SOCKET
	try:
		sock = socket.socket(socket.AF_INET, # Internet
	                         socket.SOCK_DGRAM) # UDP
		SOCKET = sock
		SOCKET.bind((udp_ip, udp_port))	
	except:
		print "Failed opening socket"
		sys.exit(0)


def send_inactive_message():
	data = {}
	data["pf"] = "/p/"+LOCATION+"/"+mac_address_prefix[-8:]
	data["bn"] = mac_address
	data["lo"] = LOCATION
	data["remove"] = True
		#Create a json object from the data
	json_data = json.dumps(data)
	return json_data


#handle ctrl-c commands so that files are wiped and sockets properly closed
def signal_handler(signal, frame):
	print "\n----------------------------------------------------------------------"
	print('FAKE SENSOR SHUTDOWN!')
	print('Closing listening UDP socket')
        #close udp socket
	SOCKET.close()
	print('Sending UDP to SDS for inactive')
	sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
	message = send_inactive_message()
	sock.sendto(message, (UDP_IP_SDS, UDP_PORT_SDS))
	print('Closing UDP socket')
	sock.close()
	print('-GOOD BYE-')
	print "-----------------------------------------------------------------------"
	sys.exit(0)


def getCharIndexes(s, ch):
    return [i for i, ltr in enumerate(s) if ltr == ch]


def get_prefix_from_interest(interest):
	res = ""
	indexes = getCharIndexes(interest, "\n")
	curr = 0
	for i in interest:
		if(i == "\n" and curr == indexes[-1]):
			break
		if(i.isdigit() or i.isalpha()):
			res +=i
		else:
			res+="/"
		curr+=1
	res = list(res)
	for i in range(0, len(res)-1):
		if(res[i] == "/" and res[i] == res[i+1]):
			res[i] = "-"
	res = "".join(res).replace("-","")
	return res


def get_cache():
	onlyfiles = [f for f in listdir("data/") if isfile(join("data/", f))]
	return onlyfiles


def createContent(prefix, filename, packet): 
	print "Creating content packet..."
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + filename + " -s " + "ndn2013" + " -o " + packet + " " + prefix)
	print "Content packet created!"


def send_udp(data, sender_ip, sock):
	numBytes = sock.sendto(data, sender_ip)	
	return numBytes


##############
###- Main -###
##############
#Handle SDS shutdown
find_data_folder()
signal.signal(signal.SIGINT, signal_handler)

while 1:
	cache = get_cache()
	udp_socket_init(UDP_IP, UDP_PORT)
	print "\n###- Waiting for interest -###"
	data, sender_ip = SOCKET.recvfrom(1024)
	print "Recieved packet from " + sender_ip[0] + " ("  + str(len(data)) + " bytes)\n"
	prefix = get_prefix_from_interest(data)
	interest = prefix.split('/')

	#get sequence number
	for x in xrange(0,len(interest)-1):
		if interest[x] == '':
			interest.remove('')
	sqn = interest[-1]+".txt"

	#check cache
	if sqn in cache:
		createContent(prefix,"data/"+sqn,"data/"+fakecontent+".ndntlv")
		o = open("data/"+fakecontent+".ndntlv","rb")
		if o:
			data = o.read(1024)
			o.close()
			#send data over udp
			print "Sending content packet..."
			numBytes = send_udp(data, sender_ip, SOCKET)
			print "Content packet for "+sqn[:4]+" sent , " + str(numBytes) + " bytes!"
		else:
			print "Error loading content object, abort"
			continue
