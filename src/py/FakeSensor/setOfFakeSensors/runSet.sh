#!/bin/bash

for (( i = 0; i < 10; i++ )); do
	mo=$(($i+1))
	gnome-terminal --title="FS_create $mo port:998$i" -x sh -c "!!; cd $CCNL_HOME/src/py/FakeSensor/setOfFakeSensors/FakeSensor$mo; pwd ; python FS_create.py" &
	sleep 0.1
	gnome-terminal --title="FS_create $mo port:998$i" -x sh -c "!!; cd $CCNL_HOME/src/py/FakeSensor/setOfFakeSensors/FakeSensor$mo; pwd ; python FS_listen.py" &
	sleep 0.3
done
