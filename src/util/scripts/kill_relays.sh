#!/bin/bash
printf "Shutting down relays..."
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.241 shutdown >/dev/null
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.225 shutdown >/dev/null
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.239 shutdown >/dev/null
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.194 shutdown >/dev/null
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.200 shutdown >/dev/null
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.195 shutdown >/dev/null
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.222 shutdown >/dev/null
printf "All relays successfully shut down!"

