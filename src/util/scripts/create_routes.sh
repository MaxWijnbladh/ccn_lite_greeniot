printf "\n#####- CREATING ROUTES -#####"
#B
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.225 newUDPface any 130.238.15.194 9695 >/dev/null
printf "\nB -> SDS"
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.225 newUDPface any 130.238.15.239 9695 >/dev/null
printf ", B -> DB"
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.225 newUDPface any 130.238.15.200 9695 >/dev/null
printf ", B -> C"
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.225 newUDPface any 130.238.15.241 9695 >/dev/null
printf ", B -> A"
printf "\n-Routes for B DONE-"
#A
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.241 newUDPface any 130.238.15.225 9695 >/dev/null
printf "\nA -> B"
printf "\n-Routes for A DONE-"
#DB
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.239 newUDPface any 130.238.15.225 9695 >/dev/null
printf "\nDB -> B"
printf "\n-Routes for DB DONE-"
#SDS
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.194 newUDPface any 130.238.15.225 9695 >/dev/null
printf "\nSDS -> B"
printf "\n-Routes for SDS DONE-"
#C
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.200 newUDPface any 130.238.15.225 9695 >/dev/null
printf "\nC -> B"
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.200 newUDPface any 130.238.15.195 9695 >/dev/null
printf ", C -> BR1"
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.200 newUDPface any 130.238.15.222 9695 >/dev/null
printf ", C -> BR2"
printf "\n-Routes for C DONE-"
#BR1
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.195 newUDPface any 130.238.15.200 9695 >/dev/null
printf "\nBR1 -> C"
printf "\n-Routes for BR1 DONE-"
#BR2
$CCNL_HOME/bin/ccn-lite-ctrl -u 130.238.15.222 newUDPface any 130.238.15.200 9695 >/dev/null
printf "\nBR2 -> C"
printf "\n-Routes for BR2 DONE-"
printf "\n#####- DONE CREATING ROUTES -#####\n"