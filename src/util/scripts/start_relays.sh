#!/bin/bash

gnome-terminal --title="Relay A" -x sh -c "sshpass -p 'relay-a' ssh relay-a@130.238.15.241 '/home/relay-a/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -x /tmp/mgmt-relay-a.sock'"
gnome-terminal --title="Relay B" -x sh -c "sshpass -p 'relayb' ssh relay-b@130.238.15.225 '/home/relay-b/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -x /tmp/mgmt-relay-a.sock'"
gnome-terminal --title="Relay C" -x sh -c "sshpass -p 'relay-c' ssh relay-c@130.238.15.200 '/home/relay-c/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -x /tmp/mgmt-relay-a.sock'"
gnome-terminal --title="Relay SDS" -x sh -c "sshpass -p 'sdssds' ssh sds@130.238.15.194 '/home/sds/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -x /tmp/mgmt-relay-a.sock -k 1'"
gnome-terminal --title="Relay BR1" -x sh -c "sshpass -p 'borderrouter1' ssh borderrouter1@130.238.15.195 '/home/borderrouter1/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -6 9696 -x /tmp/mgmt-relay-a.sock'"
gnome-terminal --title="Relay BR2" -x sh -c "sshpass -p 'borderrouter2' ssh borderrouter2@130.238.15.222 '/home/borderrouter2/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -6 9696 -x /tmp/mgmt-relay-a.sock'"
gnome-terminal --title="Relay DATABASE" -x sh -c "sshpass -p 'projectcs2016' ssh server3@130.238.15.239 '/home/server3/ccn_lite_greeniot/bin/ccn-lite-relay ccn-lite-relay -v trace -s ndn2013 -u 9695 -x /tmp/mgmt-relay-a.sock -f 1'"