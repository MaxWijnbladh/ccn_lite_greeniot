import socket
import json
import os
import subprocess
import pymongo
from pymongo import MongoClient
from bson.json_util import dumps
import StringIO
import sys
import ConfigParser
from urllib2 import urlopen
import signal
import shutil

global DATABASE
global DATABASE_IP
global DATABASE_PORT
global SDS_IP
global SDS_PREFIX
global SENSOR_LIST_TXT
global SENSOR_LIST_PREFIX
global IDENTITY_TXT
global CCN_SUITE
global UDP_IP 
global UDP_PORT 
global NEW_SENSOR
global IDENTITY_REQUEST
global SOCKET
global REMOVE_FACE_TXT
global REMOVE_FACE_PREFIX
global UPDATE_FIB_TXT
global UPDATE_FIB_PREFIX
global DATABASE_PREFIX

#handle ctrl-c commands so that files are wiped and sockets properly closed
def signal_handler(signal, frame):
	print "\n----------------------------------------------------------------------"
	print('SDS SHUTDOWN!')
	print('Closing UDP socket...')
        #close udp socket
	SOCKET.close()

        #remove temp files
	print('Removing temp files...')
	folder = 'data'
	for the_file in os.listdir(folder):
	    file_path = os.path.join(folder, the_file)
	    try:
	        if os.path.isfile(file_path) and "andrew.txt" not in file_path:
	            os.unlink(file_path)
	    except Exception as e:
	        print(e)

	print('Exited like a boss!')
	print "-----------------------------------------------------------------------"
	sys.exit(0)

#read all the global variables from the config.ini file
def getConfig():
	print "Reading config file..."
	Config = ConfigParser.ConfigParser()
	Config.read('config/config.ini')

	global DATABASE_IP
	global DATABASE_PORT
	global SDS_IP
	global SDS_PREFIX
	global SENSOR_LIST_TXT
	global SENSOR_LIST_PACKET
	global SENSOR_LIST_PREFIX
	global IDENTITY_TXT
	global CCN_SUITE 
	global UDP_IP 
	global UDP_PORT 
	global SENSOR_REQUEST
	global IDENTITY_REQUEST
	global NETWORK
	global BORDER_ROUTERS
	global REMOVE_FACE_TXT
	global REMOVE_FACE_PREFIX
	global UPDATE_FIB_TXT
	global UPDATE_FIB_PREFIX
	global DATABASE_PREFIX
	
	if Config:
		DATABASE_IP = Config.get('DATABASE', 'DATABASE_IP') 
		DATABASE_PORT = Config.get("DATABASE", 'DATABASE_PORT')
		DATABASE_PREFIX = Config.get("DATABASE", 'DATABASE_PREFIX')
		SDS_IP = urlopen('http://ip.42.pl/raw').read()
		SDS_PREFIX = Config.get("SDS", 'SDS_PREFIX')
		SENSOR_LIST_TXT = Config.get("SENSORS", 'SENSOR_LIST_TXT')
		SENSOR_LIST_PREFIX = Config.get("SENSORS", 'SENSOR_LIST_PREFIX')
		IDENTITY_TXT = Config.get("IDENTITY", 'IDENTITY_TXT')
		CCN_SUITE = Config.get("CCN", 'CCN_SUITE')
		UDP_IP = Config.get("LISTENING_SOCKET", 'UDP_IP')
		REMOVE_FACE_TXT = (Config.get("RELAY", 'REMOVE_FACE_TXT'))
		REMOVE_FACE_PREFIX = (Config.get("RELAY", 'REMOVE_FACE_PREFIX'))
		UDP_PORT = int(Config.get("LISTENING_SOCKET", 'UDP_PORT'))
		SENSOR_REQUEST = (Config.get("PREFIXES", 'SENSOR_REQUEST'))
		IDENTITY_REQUEST = (Config.get("PREFIXES", 'IDENTITY_REQUEST'))
		UPDATE_FIB_TXT = (Config.get("RELAY", 'UPDATE_FIB_TXT'))
		UPDATE_FIB_PREFIX = (Config.get("RELAY", 'UPDATE_FIB_PREFIX'))
	else:
		print "Couldn't read config file"
		sys.exit(0)
	#remove existing files so we don't send incorrect data (necessary?)
	try:
		os.remove(SENSOR_LIST_TXT)
		os.remove(SENSOR_LIST_PACKET)
	except:
		pass

#write the string data to the filepath path 
def write_file(data, path):
	try:
		fd = open(path, 'w')
		fd.write(data)
		fd.close()
	except:
		print "Failed writing to file " + path

#remove the face to peer from all relays in the network
def remove_peer_from_relays(relay):
	data = ""
	buf = 1024
	content = relay + "\0"
	try:
		os.remove(REMOVE_FACE_TXT)
	except:
		pass

	write_file(content, REMOVE_FACE_TXT)

	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + REMOVE_FACE_TXT + " -s " + CCN_SUITE + " " + REMOVE_FACE_PREFIX)
	data = process.read()
	#go through each relay in the network, check if it has a link to peer. if so, remove that link
	for node, peer in NETWORK.iteritems():
		if relay in peer:
			print "Removing face to " + relay + " from " + node
			dest = (node, 9695)
			send_udp(data, dest)

#initilisate a udp socket for sending and recieiving udp traffic
def udp_socket_init():
	global SOCKET
	try:
		sock = socket.socket(socket.AF_INET, # Internet
	                         socket.SOCK_DGRAM) # UDP
		SOCKET = sock
		SOCKET.bind((UDP_IP, UDP_PORT))	
	except:
		print "Failed opening socket"
		sys.exit(0)

#create a content packet with the sensors from the database
def create_content_packet():
	content = 1024
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + SENSOR_LIST_TXT + " -s " + CCN_SUITE + " " + SENSOR_LIST_PREFIX)
	content = process.read()
	print "Content packet created!"
	return content

#send data to destination dest
def send_udp(data, dest):
	numBytes = SOCKET.sendto(data, dest)	
	return numBytes

#check if data is valid JSON
def check_datatype(data):
	try:
		json_object = json.loads(data)
  	except ValueError, e:
  		return False
  	return True 

#parse json and write content to file  	
def create_sensorlist(sensors):
	res = str(dumps(sensors))
	write_file(res, SENSOR_LIST_TXT)

#print sensor information
def print_sensor(sensor):
	if sensor:
		print sensor

#add a relay to the network
def add_to_network(relay):
	global NETWORK
	if relay:
		print "Relay register request (" + relay + ")"
		print "Adding relay to network"
		#Add relay to network graph
		if relay in NETWORK:
			print "Relay already registered"
		else:
			NETWORK[relay] = []
	else:
		print "Failed adding relay"

#add link between relay and peer
def add_peer_to_relay(relay, peer):
	print "New link request!"
	peer = peer.encode('utf8')
	if relay and peer:
		global NETWORK
		if relay in NETWORK:
			if peer in NETWORK[relay]:
				print "Link between " + relay + " and " + peer + " already exists!"
			else:
				NETWORK[relay].append(peer)
				print "Link created!"
				#add a path to db from this relay
				add_path_to_db(relay)
				add_path_to_sds(relay)
		else:
			print relay + " not in network, adding it to network now..."
			add_to_network(relay)
			add_peer_to_relay(relay, peer)
	else:
		print "Failed adding link"

#find the shortest path between start and end in the graph 
def find_path(graph, start, end, path=[]):

    path = path + [start]
    if start == end:
        return path
    if not graph.has_key(start):
        return None
    for node in graph[start]:
        if node not in path:
            newpath = find_path(graph, node, end, path)
            if newpath: return newpath
    return None


#add a prefix to a borderrouter
def add_prefix_to_network(prefix, borderRouter):
	fib_entry = extract_fib_entry(prefix)
	if fib_entry and borderRouter:
		if not borderRouter in BORDER_ROUTERS:
			BORDER_ROUTERS[borderRouter] = []

		if fib_entry in BORDER_ROUTERS[borderRouter]:
			print "Prefix already registrered in " + borderRouter
			return None
		else:
			BORDER_ROUTERS[borderRouter].append(prefix)
			print fib_entry + " added to " + borderRouter
			print "Adding " + fib_entry + " to relays in network"
			populate_fibs(fib_entry, borderRouter)
	else:
		print "Failed adding prefix to network"

#populate the fib table of each relay in the network with a path to the borderouter generating the prefix
def populate_fibs(prefix, borderRouter):
	path = []

	for relay in NETWORK:
		path = []
		path = find_path(NETWORK, relay, borderRouter, path)
		#print path
		if path != None:
			target = path[0]
			if (1 < len(path)):
				peer = path[1]
			else:
				continue
			path = []
			update_fib(prefix, target, peer)

#update fib table of relay with path to prefix through peer
def update_fib(prefix, relay, peer):
		data = None
		buf = 1024
		content = prefix + "|" + peer + "\0"
		write_file(content, UPDATE_FIB_TXT)	
		process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + UPDATE_FIB_TXT + " -s " + CCN_SUITE  + " " + UPDATE_FIB_PREFIX)
		data = process.read()
		dest = (relay, 9695)
		if data:
			send_udp(data, dest)
		else:
			print "Failed sending FIB update"
#remove a relay from the network
def remove_relay_from_network(relay):
	if relay:
		if relay in NETWORK:
			NETWORK.pop(relay)
		if relay in BORDER_ROUTERS:
			BORDER_ROUTERS.pop(relay)
	else:
		print "Failed removing relay"

	for node, peer in NETWORK.iteritems():
		if relay in peer:
			peer.remove(relay)
	print "Relay removed from network"

#Get IP address automatically for send_identity
def get_ip_address():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]

#send sds ip to sender_ip
def send_identity(sender_ip): 
	print "SDS identity request"
	buf = 1024
	#write_file(SDS_IP, IDENTITY_TXT)
	write_file(get_ip_address(), IDENTITY_TXT)
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + IDENTITY_TXT + " -s " + CCN_SUITE + " " + IDENTITY_REQUEST)
	data = process.read()
	#send it to local relay
	print "Sending identity packet..."
	if data:
		numBytes = send_udp(data, sender_ip)
		print "Identity packet sent! (" + str(numBytes) + " bytes)" 
	else:
		print "Failed sending identity packet"
	 
#add a sensor 
def add_sensor(sensor, borderRouter):
	#if new sensor information, add it to the database
	print "New sensor request!"

	if sensor and borderRouter:
		#print_sensor(sensor)
		print "Adding to database..."
		for x in xrange(1,len(sensor)+1):
			jsonChunk = {}
			for key, value in sensor[0].iteritems():
				jsonChunk[key] = value
			try:
				print jsonChunk['bn']
				jsonChunk["nfn"] = NFN_PREFIXES
				DATABASE.update_one({"bn":jsonChunk["bn"]},{'$set': jsonChunk}, upsert=True)
				print "Sensor '" + jsonChunk['pf'] + "' added to database!"
				add_prefix_to_network(extract_fib_entry(jsonChunk['pf']), borderRouter)
			except pymongo.errors.DuplicateKeyError:
				print jsonChunk['bn']
				print "Sensor already in database, setting it as active"
				set_sensor_status(jsonChunk, True)
				#return None
				continue
		dest = (DATABASE_IP, 9999)
		send_udp("Sensor updated",dest)
	else: 
		print "Failed adding sensor"

def send_sensorlist(destination):
	print "Available sensor request!"
	#get sensors from db
	try:
		sensors = list(DATABASE.find({}, {"a": 1, "_id": 0, "pf": 1, "lo": 1,  "bsn": 1, "but": 1, "bt": 1, "nfn": 1}))
	except:
		print "Couldn't fetch sensors from sensor database!"

	print "Sensors fetched!"
	#convert entries into string format
	create_sensorlist(sensors);
	content = create_content_packet()

	if content:
		#send data over udp
		numBytes = send_udp(content, destination)
		print "Content packet sent, " + str(numBytes) + " bytes!"
	else:
		print "Error loading content object, abort"

def get_prefix_from_interest(interest): #Sami
	try:
	    for x in xrange(1,len(interest)):
	        #print interest[x]
	        pass
	    extStr = interest[interest.index(""):interest.index("\n")]
	    extStr = map(lambda c: str(c) if c.isalpha() else '-', extStr)
	    splitted = ''.join(extStr).split("-")
	    splitted = filter(lambda s: s != "", splitted)
	    realPrefix = ""
	    for s in splitted:
	        realPrefix += "/" + s
	    return str(realPrefix)
	except:
		print "Unable to parse prefix"
		return None

def set_sensor_status(sensor, status):
	sensor_mac = sensor['bn']
	#DATABASE.update({'bn': sensor_mac}, {'a': status})
	DATABASE.update_one({'bn': sensor_mac},{'$set': {'a': status, 'bsn': '1'}}, upsert=False)
	#result = DATABASE.delete_many({"sensor_mac": sensor_mac})
	dest = (DATABASE_IP, 9999)
	send_udp("Sensor status updated",dest)
	if status:
		print sensor_mac + " set to active"
	else:
		print sensor_mac + " set to inactive"
		
#Extract the first part from a prefix
def extract_fib_entry(prefix):
	fib_entry = '/'.join(prefix.split('/')[:4])
	return fib_entry

def print_network():
	if NETWORK:
		print("" + "\n".join("{} -> {}".format(k, v) for k, v in NETWORK.items()) + "")

def add_path_to_db(relay):

	path = []

	path = find_path(NETWORK, relay, DATABASE_IP, path)
	if path != None:
		target = path[0]
		if (1 < len(path)):
			peer = path[1]
		else:
			peer = path[0]			
		path = []
		update_fib(DATABASE_PREFIX, target, peer)
	else:
		print "No path found from " + relay + " to " + DATABASE_IP 

def add_path_to_sds(relay):
	
	path = []

	path = find_path(NETWORK, relay, SDS_IP, path)
	if path != None:
		target = path[0]
		if (1 < len(path)):
			peer = path[1]
		else:
			return None		
		path = []
		update_fib(SDS_PREFIX, target, peer)
	else:
		print "No path found from " + relay + " to " + SDS_IP 

def add_nfn_prefix(prefix):
	global NFN_PREFIXES
	print prefix
 	response_prefix = '%s' % prefix 
	print "Adding new NFN"
	nfn_prefix = prefix.split("/sds/namedfunction")[1] 
	print "NFN: " + nfn_prefix
	NFN_PREFIXES.append(nfn_prefix)
	print "NFN added!"
	message = "NFN added!"
	path = "data/NFNconfirmation.txt"
	write_file(message, path)
	print response_prefix
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + path + " -s " + CCN_SUITE  + " " + response_prefix)
	data = process.read()
	send_udp(data, ("127.0.0.1", 9695))

def main():
	global DATABASE
	global NETWORK
	global BORDER_ROUTERS
	global NFN_PREFIXES
	NFN_PREFIXES = []

	#Handle SDS shutdown
	signal.signal(signal.SIGINT, signal_handler)

	sender_ip = None;
	isJSON = None
	sensorList = None
	buf = 1024
	
	NETWORK = {}
	BORDER_ROUTERS = {}

	print "\n\n-----------------------------------------------------------------------"
	#init global variables
	getConfig()

	#init database
	client = MongoClient(DATABASE_IP+":"+DATABASE_PORT, serverSelectionTimeoutMS=3000)
	try:
		client.server_info()
	except:
		print "Couldn't connect to sensor database, exiting"
		sys.exit(0)
	print "Connected to database! ("+DATABASE_IP+":"+DATABASE_PORT+")"
	db = client.test
	DATABASE = db.sensors
	#DATABASE.drop() #NOT THIS!! All the history is in same database!!
	DATABASE.ensure_index("bn", unique=True)

	#create listening socket
	udp_socket_init()
	print "Listening socket opened! (127.0.0.1:"+str(UDP_PORT)+")"
	#send path to SDS to local relay
	#send_identity(sock, ("0.0.0.0", 9695))
	
	#fancy terminal ascii stuff
	print "\n#######################################################################\n"
	print "               CCN Network Directory Service"
	print "                     \x1B[3mConnecting people\x1B[23m\n"
	print "#######################################################################"
	
	while True:

		#Wait for packet 
		print "-----------------------------------------------------------------------"
		print "Waiting for packet..."

		
		data, sender_ip = SOCKET.recvfrom(1024) # buffer size is 1024 bytes
	
		print "Recieved packet from " + sender_ip[0] + " ("  + str(len(data)) + " bytes)"
		#check whether it's JSON packet or CCN packet
		
		isJSON = check_datatype(data)

		if isJSON:
		 	JSON_DATA = json.loads(data)
			if "Status" in JSON_DATA:
				if JSON_DATA['Status'] == "Off":
					print "Relay deregister request (" + sender_ip[0] + ")"
					remove_peer_from_relays(sender_ip[0])
					remove_relay_from_network(sender_ip[0])
				elif JSON_DATA['Status'] == "On":
					add_to_network(sender_ip[0])
				print_network()
			elif "Peer" in JSON_DATA:
				add_peer_to_relay(sender_ip[0], JSON_DATA['Peer'])
				print_network()
			elif "remove" in JSON_DATA:
				print "Sensor inactivation request"
				set_sensor_status(JSON_DATA, False)
			else:
				add_sensor(JSON_DATA, sender_ip[0])

		else: #This is a CCN packet
			interest = get_prefix_from_interest(data) 
			if interest != None:
				#if it's a requst for all sensors, get it from the database and send it back
				if interest == SENSOR_REQUEST:
					send_sensorlist(sender_ip)
				#if it's a request for the IP of the SDS, create content packet out of it and send it
				elif interest == IDENTITY_REQUEST: 
					send_identity(sender_ip)
				elif "namedfunction" in interest:
					print interest
					add_nfn_prefix(interest)
			else:
				print "Discarding packet"

if __name__ == "__main__":
    main()