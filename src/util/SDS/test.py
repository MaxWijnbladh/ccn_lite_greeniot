import socket
import json
import os
import subprocess
import pymongo
from pymongo import MongoClient
from bson.json_util import dumps
import StringIO
import sys
import ConfigParser
from urllib2 import urlopen
import signal
import shutil


def write_file(data, path):
	try:
		fd = open(path, 'w')
		fd.write(data)
		fd.close()
	except:
		print "Failed writing to file " + path

def main():
	data = None
	UPDATE_FIB_TXT = "addentry.txt"
	UPDATE_FIB_PREFIX = "/ccnx//updateFIB"
	suite = "ccnb"
	content = "/newprefix|130.238.15.214\0"
	write_file(content, UPDATE_FIB_TXT)	
	process = os.popen("$CCNL_HOME/src/util/ccn-lite-mkC -i " + UPDATE_FIB_TXT + " -s " + suite  + " " + UPDATE_FIB_PREFIX)
	data = process.read()
	try:
		sock = socket.socket(socket.AF_INET, # Internet
	                         socket.SOCK_DGRAM) # UDP
		SOCKET = sock
		SOCKET.bind(("127.0.0.1", 5555))	
	except:
		print "Failed opening socket"
		sys.exit(0)
	SOCKET.sendto(data, ("130.238.15.194", 9695))




if __name__ == "__main__":
    main()