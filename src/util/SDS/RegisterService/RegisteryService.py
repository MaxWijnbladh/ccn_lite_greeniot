#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import json
import socket
import argparse
import datetime
import ConfigParser

# ----------------------------------------------------------------------
# #-process argv-#

parser = argparse.ArgumentParser(description='Registery service for keeping track of the sensors connected to border router on this computer. (Uppsala University ProjectCS 2016). Arguments are meant for testing for permanent changes modify the config file at config/rs_config.ini')
parser.add_argument('-s','--suite', type=str, help='Suite used in CCN-lite commands', required=False, default='')
parser.add_argument('-u','--relAdd', type=str, help='UDP addr of access router (i.e.: 127.0.0.1/9695)', required=False, default='')
parser.add_argument('-d','--sds', type=str, help='SDS address', required=False, default='')
parser.add_argument('-l','--location', type=str, help='Location of the sensor', required=False, default='')
parser.add_argument('-x','--relay', type=str, help='Path to ccn-lite relay socket (i.e.: /tmp/mgmt-relay-a.sock)', required=False, default='')
args = vars(parser.parse_args())



global LOCATION
global LOCATIONFULL
global CCN_SUITE
global RELAY_ADDRESS
global BORDER_RELAY_NAME
global UDP_IP
global UDP_PORT
global BR_IP_LIST
global isPath



#--------------------------------------------------------------------------------------------------
#-Define Functions-#

def getConfig():

	print "Reading config file..."
	Config = ConfigParser.ConfigParser()
	Config.read('config/rs_config.ini')

	global LOCATION
	global LOCATIONFULL
	global CCN_SUITE
	global RELAY_ADDRESS
	global BORDER_RELAY_NAME
	global UDP_IP
	global UDP_PORT
	global BR_IP_LIST
	global isPath

	if args['location'] != '':
		LOCATION = args['location']
	else:
		LOCATION = Config.get('BORDER_ROUTER', 'LOCATION')
	LOCATIONFULL = Config.get('BORDER_ROUTER', 'LOCATIONFULL')
	if args['suite'] != '':
		CCN_SUITE = args['suite']
	else:
		CCN_SUITE = Config.get('CCN', 'CCN_SUITE')
	if args['relAdd'] != '':
		RELAY_ADDRESS = args['relAdd']
	else:
		RELAY_ADDRESS = Config.get('CCN', 'RELAY_ADDRESS')
	if args['relay'] != '':
		BORDER_RELAY_NAME = args['relay']
	else:
		BORDER_RELAY_NAME = Config.get('CCN', 'BORDER_RELAY_NAME')
	if args['sds'] != '':
		UDP_IP = args['sds']
	else:
		UDP_IP = Config.get('UDP_SOCKET', 'UDP_IP')
	UDP_PORT = Config.get('UDP_SOCKET', 'UDP_PORT')
	BR_IP_LIST = Config.get('FILES', 'BR_IP_LIST')

	if os.path.isdir(BR_IP_LIST[:-18]) == False:
		sys.exit("\nERROR: Could not locate activesensors.txt! Please check config/rs_config.ini for the 'BR_IP_LIST' path.\n")
	if LOCATION == 'NOTDEFINED':
		sys.exit("\nERROR: Location not defined in config/rs_config.ini. Please check all the settings there before trying to run RS!\n")
	if "/" in BORDER_RELAY_NAME:
		isPath = True
	else:
		isPath = False
	LOCATION = LOCATION.lower()
	UDP_PORT = int(UDP_PORT)
	print "Config file LOADED"



#Function to get the ip list from Border rotuer
def get_ip_list():
	# list_of_ipv6 = ['fd02::212:4b00:7a8:df86/fe80::212:4b00:7a8:df86']
	list_of_ipv6 = open(BR_IP_LIST)
	lines = list_of_ipv6.read().split('\n')
	list_of_ipv6.close()
	del lines[-1]
	for line in lines:
		if line != '':
			lineArr = line.split('/')
			if lineArr[0][4:] != lineArr[1][4:]:
				lines[lines.index(line)] = ''
			else:
				lines[lines.index(line)] = lineArr[0]
	for line in lines:
		if line == '':
			lines.remove(line)
	print "Current sensors:"
	print lines
	return lines



#Read from sensor ip file
def get_old_ip_list():
	try:
		old_ip_list = open("temp_sensor_ip_list_backup.txt")
		lines = old_ip_list.read().split('\n')
		old_ip_list.close()
		del lines[-1]
		print "Sensors in previous iteration:"
		print lines
		return lines
	except Exception, e:
		old_ip_list = []
		#print old_ip_list
		return old_ip_list



#Function to translate ipv6 to mac address
def ipv62mac(ipv6):
    # remove subnet info if given
    subnetIndex = ipv6.find("/")
    if subnetIndex != -1:
        ipv6 = ipv6[:subnetIndex]

    ipv6Parts = ipv6.split(":")
    macParts = []
    for ipv6Part in ipv6Parts[-4:]:
        while len(ipv6Part) < 4:
            ipv6Part = "0" + ipv6Part
        macParts.append(ipv6Part[:2])
        macParts.append(ipv6Part[-2:])

    # modify parts to match MAC value
    macParts[0] = "%02x" % (int(macParts[0], 16) ^ 2)
    del macParts[4]
    del macParts[3]

    return ":".join(macParts)



#Send UDP message
def send_udp(destination,port,message):
		#Set SDS address and port
	#UDP_IP = "130.238.15.208" #TEMP
	UDP_IP = destination
	UDP_PORT = port
	MESSAGE = message
		
		#Send UDP message
	sock = socket.socket(socket.AF_INET, # Internet
	socket.SOCK_DGRAM) # UDP
	sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

#WaitUntil funciton. Wait 10 sec tocheck the active sensor list again
def wait_until():
	getout = 1
	while (getout == 1):
		date = datetime.datetime.now()
		if (date.second) % 10 == 0:
			getout = 0
		date += datetime.timedelta(seconds=1)
		time.sleep (1)
		
	 

#--------------------------------------------------------------------------------------------------
#-Main-#

#init global variables
getConfig()

#Get SDS IPv4 if not defined
if (UDP_IP == 'auto'):
	UDP_IP = os.popen('$CCNL_HOME/bin/ccn-lite-peek -s '+CCN_SUITE+' -u '+RELAY_ADDRESS+' /sds/identity | $CCNL_HOME/bin/ccn-lite-pktdump -f2').read()
	print "IP of SDS is: "+UDP_IP  #DEBUG
	UDP_PORT = UDP_PORT
	if (UDP_IP == ''):
		sys.exit("\nERROR: Could not fetch IPv4 for Sensor Directory Service!\n   Note: You can try to set it manyally with argument -d\n")
else:#TEMP Remember to delete this for the final version!!!
	print "Hardcoded SDS ip in use: "+UDP_IP  #DEBUG



#Start loop
while (1):
	print ""
	print "----------"+str(datetime.datetime.now().year)+"-"+str(datetime.datetime.now().month)+"-"+str(datetime.datetime.now().day)+"-"+str(datetime.datetime.now().hour)+"-"+str(datetime.datetime.now().minute)+"-"+str(datetime.datetime.now().second)+"---------"
	print "CHECKING FOR UPDATES FROM BORDER ROUTER"
	print "---------------------------------------"

		#Get the ip's from the list
	list_of_ipv6 = get_ip_list()
		#Get the OLD ip's from the file if exsists
	old_ip_list = get_old_ip_list()
		#Filter old ip's out of the ipv6 list
	new_ip_in_list = [i for i in list_of_ipv6 if i not in old_ip_list]
	print "\nADD THESE: "+str(new_ip_in_list)  #DEBUG
	remove_ip_in_list = [i for i in old_ip_list if i not in list_of_ipv6]
	print "REMOVE THESE: "+str(remove_ip_in_list)+"\n"  #DEBUG

		#Refresh the temp file of the border router connections
	refresh_file = open('temp_sensor_ip_list_backup.txt', 'w')
	for item in list_of_ipv6:
		print>>refresh_file, item
	refresh_file.close()

		#define message container for udp
	jasonMessageTemp = []

		#Do stuff for only new ip addersses
	if new_ip_in_list != [] and new_ip_in_list != ['']:

			#For every new ip do as follows
		for sensor in new_ip_in_list:
			print "DEBUG: Dealing with sensor "+sensor  #DEBUG

			#Translate mac address from IPv6
			mac_address = ipv62mac(sensor)
				#Take out : from mac address for the prefix
			mac_address_prefix = mac_address.translate(None, ':')

				#Check relay fases for dublicate
			if isPath:
				checkFaces = os.popen('$CCNL_HOME/bin/ccn-lite-ctrl -x '+BORDER_RELAY_NAME+' getfaces').read()
			else:
				checkFaces = os.popen('$CCNL_HOME/bin/ccn-lite-ctrl -u '+BORDER_RELAY_NAME+' getfaces').read()

			checkFaces = checkFaces.split('-')
			
			dont_add_face = 0
			for face in checkFaces:
				if(sensor in face):
					dont_add_face = 1

			try:
				if (dont_add_face == 0):
							
						#Create new ipv6 face to new sensor
					if isPath:
						face_id = os.popen('$CCNL_HOME/bin/ccn-lite-ctrl -x '+BORDER_RELAY_NAME+' newUDP6face any '+sensor+' 1001 | $CCNL_HOME/bin/ccn-lite-ccnb2xml').read()
					else:
						face_id = os.popen('$CCNL_HOME/bin/ccn-lite-ctrl -u '+BORDER_RELAY_NAME+' newUDP6face any '+sensor+' 1001 | $CCNL_HOME/bin/ccn-lite-ccnb2xml').read()
						#Take out the face ID from teh face_id. (Oh goosh! It looks hidious!)
					face_id = face_id.split('FACEID>')
					face_id = face_id[1].split('<')
					face_id = face_id[0]

						#Create registery prefix route
					prefix = "/p/" + mac_address_prefix[-8:]
					#prefix = "/p/4ba8df86/"
						#Create new prefix route for sensor registration
					if isPath:
						os.system('$CCNL_HOME/bin/ccn-lite-ctrl -x '+BORDER_RELAY_NAME+' prefixreg '+prefix+' '+face_id+' '+CCN_SUITE+' | $CCNL_HOME/bin/ccn-lite-ccnb2xml')
					else:
						os.system('$CCNL_HOME/bin/ccn-lite-ctrl -u '+BORDER_RELAY_NAME+' prefixreg '+prefix+' '+face_id+' '+CCN_SUITE+' | $CCNL_HOME/bin/ccn-lite-ccnb2xml')

						#Prefix routes to the sensor values for relay
					prefix = "/p/"+LOCATION+"/"+mac_address_prefix[-8:]
					#prefix = "/p/"+LOCATION
					if isPath:
						os.system('$CCNL_HOME/bin/ccn-lite-ctrl -x '+BORDER_RELAY_NAME+' prefixreg '+prefix+' '+face_id+' '+CCN_SUITE+' | $CCNL_HOME/bin/ccn-lite-ccnb2xml')
					else:
						os.system('$CCNL_HOME/bin/ccn-lite-ctrl -u '+BORDER_RELAY_NAME+' prefixreg '+prefix+' '+face_id+' '+CCN_SUITE+' | $CCNL_HOME/bin/ccn-lite-ccnb2xml')
			except:
				continue
				
				#Get sensor information content object from sensor
			prefix = "/p/" + mac_address_prefix[-8:] +"/regist/"
			#print prefix  #DEBUG
			content = os.popen('$CCNL_HOME/bin/ccn-lite-peek -s '+CCN_SUITE+' -u '+RELAY_ADDRESS+' ' + prefix + ' | $CCNL_HOME/bin/ccn-lite-pktdump -f2').read()
			#content = "TI CC2650 SensorTag-Tem,Lig,Hum-10-"


				#Send location to sensor
			prefix = prefix+LOCATION
			#prefix = "/p/4ba8df86/regist/foobar"
			#print prefix  #DEBUG
			os.system('$CCNL_HOME/bin/ccn-lite-peek -s '+CCN_SUITE+' -u '+RELAY_ADDRESS+' -w 1.0 ' + prefix + ' | $CCNL_HOME/bin/ccn-lite-pktdump -f2')



				#Parse content and ad it to list of data
			#if (content != "" or peak_values != ""): #REMOVE IF NOT USED!!
			if (content != ""):
				content = content.split('-')
				#peak_values = peak_values.split('-')
				data = {}
				data["bn"] = mac_address
				data["a"] = True
				data["bt"] = time.time()
				data["pf"] = "/p/"+LOCATION+"/"+mac_address_prefix[-8:]
				data["lo"] = LOCATIONFULL
				data["md"] = content[0]
				data["but"] = int(content[1])
				data["bsn"] = "1"
				data["ver"] = "1"

				Lvalue = {}
				Lvalue["n"] = "Light"
				Lvalue["u"] = "lx"
				Lvalue["v"] = float(content[2])
				Lvalue["t"] = time.time()
				Lvalue["sn"] = "1"

				Tvalue = {}
				Tvalue["n"] = "Temperature"
				Tvalue["u"] = "C"
				Tvalue["v"] = float(content[3])
				Tvalue["t"] = time.time()
				Tvalue["sn"] = "1"

				Hvalue = {}
				Hvalue["n"] = "Humidity"
				Hvalue["u"] = "%"
				Hvalue["v"] = float(content[4])
				Hvalue["t"] = time.time()
				Hvalue["sn"] = "1"
		
				data["e"] = [Tvalue,Lvalue,Hvalue]
				#print data  #DEBUG
				jasonMessageTemp.append(data)
					#Create a json object from the data
				json_data = json.dumps(jasonMessageTemp)
				#print ""
				#print "JSON data:"
				#print json_data
				#print ""
				
				send_udp(UDP_IP,UDP_PORT,json_data)
				print "Add new ip's: DONE"
			else:
				print "\nERROR: Failed to receive content from sensor: \n       Mac: " + mac_address + "\n       IPv6: " + sensor + "\n"
				continue
			time.sleep (1)

		

	if remove_ip_in_list != [] and remove_ip_in_list != ['']:
		for sensor in remove_ip_in_list:
			print "DEBUG: removing "+sensor  #DEBUG
				#Translate mac address from IPv6
			mac_address = ipv62mac(sensor)
				#Take out : from mac address for the prefix
			mac_address_prefix = mac_address.translate(None, ':')

			data = {}
			data["pf"] = "/p/"+LOCATION+"/"+mac_address_prefix[-8:]
			data["bn"] = mac_address
			data["lo"] = LOCATION
			data["remove"] = True
			#print data  #DEBUG
				#Create a json object from the data
			json_data = json.dumps(data)
			#print ""
			#print "JSON data:"
			#print json_data
			#print ""
			
			send_udp(UDP_IP,UDP_PORT,json_data)
			print "Remove old ip's: DONE"
	
	wait_until()